import os
import random
import os, uuid, requests

from onepointone import settings

def download_image(download_link):
    response = requests.get(download_link)
    random_name = uuid.uuid4().hex + ".png"
    folder_name = "profile"
    
    absolute_file_path = os.path.join(settings.MEDIA_ROOT, folder_name, random_name)
    relative_path = os.path.join(folder_name, random_name)
    with open(absolute_file_path, 'wb') as f:
        f.write(response.content)
    
    print("Relative Path == ", relative_path)
    return relative_path

    