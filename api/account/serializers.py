from api.account.models import User
from rest_framework import serializers
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        exclude = ['is_staff', 'is_superuser']
    
    
class FileUploadSerializer(serializers.Serializer):
    file = serializers.FileField()    
    
        
class LoginSerializer(TokenObtainPairSerializer):
    
    def validate(self, attr):
        data = super().validate(attr)
        response = {'status': 'SUCCESS','data': data }
        return response
    