from django.urls import  path
from api.account.views import LoginAPIView, UserUploadByCSVAPIView, UserViewset
from rest_framework_simplejwt import views as jwt_views

trailing_slash = False

urlpatterns = [    
    path('login/', LoginAPIView.as_view(), name='login'),
    # path('login/', jwt_views.TokenObtainPairView.as_view(), name='login'),
    path('token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
    path('register/', UserViewset.as_view({'post': 'create'}), name='register'),
    path('employee-list/', UserViewset.as_view({'get': 'list'}), name='employee-list'),
    path('upload-user/', UserUploadByCSVAPIView.as_view(), name='upload-user'),
]