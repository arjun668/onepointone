from datetime import datetime
import io
import os
from django.shortcuts import render

from django.urls import include, path
from api.account.utils import download_image
from onepointone import settings

from rest_framework_simplejwt import views as jwt_views
from rest_framework import viewsets, status
from rest_framework.views import APIView
from rest_framework_simplejwt import views as jwt_views
from rest_framework.response import Response
from rest_framework.permissions import AllowAny


from api.account.models import User
from api.account.serializers import LoginSerializer, UserSerializer

from rest_framework.exceptions import AuthenticationFailed
from rest_framework_simplejwt.exceptions import TokenError
from django.contrib.auth.hashers import make_password

import urllib.request


import csv

class UserViewset(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [AllowAny]
    
    def create(self, request, *args, **kwargs):
        request.data._mutable = True
        request.data['password'] = make_password(request.data['password'])
        date_of_joining = request.data.get('date_of_joining')
        date_of_birth = request.data.get('date_of_birth')
        request.data['date_of_joining'] = datetime.strptime(date_of_joining, "%d-%m-%Y").strftime("%Y-%m-%d")
        request.data['date_of_birth'] = datetime.strptime(date_of_birth, "%d-%m-%Y").strftime("%Y-%m-%d")
        return super().create(request, *args, **kwargs)
    
    
class LoginAPIView(jwt_views.TokenObtainPairView):
    serializer_class = LoginSerializer
    
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        try:
            serializer.is_valid(raise_exception=True)
        except AuthenticationFailed as e:
            response = {'status': 'FAILED', 'data': {'error': e.args[0]} }
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        except TokenError as e:
            response = {'status': 'FAILED', 'data': {'error': e.args[0]} }
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
            
        return Response(serializer.validated_data, status=status.HTTP_200_OK)
    
    
class UserUploadByCSVAPIView(APIView):   
    permission_classes = [AllowAny] 
    def post(self, request, *args, **kwargs):
        predefined_columns = ['Name','Email', 'Password', 'Date Of Birth', 'Date Of Joining', 'Gender', 'Designation','Picture']
        
        with io.TextIOWrapper(request.FILES["file"], encoding="utf-8", newline='\n') as text_file:
            rows = csv.DictReader(text_file, delimiter=',')
            rows = list(rows)
            errors = []
            message  = 'File processed successfully'
            if rows:
                csv_colums = rows[0].keys()
                missing_columns = [k for k in predefined_columns if not k in csv_colums]
                if missing_columns:
                    response = {
                        'status': 'FAILED',
                        'data': {'error': 'Please add missing columns in CSV file','missing_columns': ', '.join(missing_columns)}
                    }
                    return Response(status=200, data=response)
                                                              
                for row in rows:
                    picture = row.get('Picture')
                    name = row.get('Name')
                    email = row.get('Email')
                    password = row.get('Password')
                    date_of_birth = row.get('Date Of Birth')
                    date_of_joining = row.get('Date Of Joining')
                    gender = row.get('Gender')
                    designation = row.get('Designation')
                    
                    date_of_joining = datetime.strptime(date_of_joining, "%d-%m-%Y").strftime("%Y-%m-%d")
                    date_of_birth = datetime.strptime(date_of_birth, "%d-%m-%Y").strftime("%Y-%m-%d")
                    
                    data = {
                        'name': name,
                        'email': email,
                        'password': make_password(password),
                        'designation': designation,
                        'date_of_birth': date_of_birth,
                        'date_of_joining': date_of_joining,
                        'gender': gender, 
                    }

                    serializer_obj = UserSerializer(data=data)
                    if serializer_obj.is_valid():
                        obj = serializer_obj.save()
                        obj.picture = download_image(picture)
                        obj.save()
                    else:
                        error = {'email': email,'message': serializer_obj.errors}
                        errors.append(error)
                        message = 'File processed successfully with below erros. kindly check and upload again'
                        
                response = {'status': 'SUCCESS', 'data': {'message': message}}        
                if errors:
                    response['data']['errors'] = errors
        
        
        return Response(status=200, data=response)